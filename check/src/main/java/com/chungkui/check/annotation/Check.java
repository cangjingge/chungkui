package com.chungkui.check.annotation;


import com.chungkui.check.core.MsgModel;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Copyright (C), 2002-2019,草帽团开发团队
 * 〈入参校验注解〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @fileName: Check.java
 * @date: 2019/6/26 13:54
 * @see [相关类/方法](可选)
 * @since [产品/模块版本] (可选)
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Check {
    /**
     * 功能描述: <br>
     * 〈校验数组类型配置〉
     *
     * @return
     * @throws
     * @Author jason
     * @date 2019/6/27 12:58
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    String[] value() default {};

    /**
     * 用于解决嵌套参数校验和数组类型的校验
     *
     * @return
     */
    String rules() default "";

    /**
     * 功能描述: <br>
     * 〈校验模式〉
     *
     * @return java.lang.String
     * @throws
     * @Author jason
     * @date 2019/6/27 11:59
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    String model() default "";

    /**
     * 功能描述: <br>
     * 〈支持配置从配置中心获取，配置的是整个rules〉
     *
     * @return java.lang.String
     * @throws
     * @Author jason
     * @date 2019/6/28 13:13
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    String remote() default "";

    /**
     * 全局触发条件
     *
     * @return
     */
    String condition() default "";

    /**
     * 功能描述: <br>
     * 〈支持配置从scm获取，配置的是整个rules〉
     *
     * @return java.lang.String
     * @throws
     * @Author jason
     * @date 2019/6/28 13:13
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    String msgTemp() default "";

    /**
     * 功能描述: <br>
     * 〈重复提交防火墙〉
     *
     * @param
     * @return java.lang.String
     * @throws
     * @Author jason
     * @date 2019/9/27 23:51
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    String reSubmitWall() default "";

    /**
     * 功能描述: <br>
     * 〈流控〉
     *
     * @param
     * @return java.lang.String
     * @throws
     * @Author jason
     * @date 2019/9/27 23:51
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    String trafficControl() default "";

    String[] rateLimters() default {};

    String[] redisRateLimters() default {};

    /**
     * 功能描述: <br>
     * 〈消息通知模式〉
     *
     * @Author jason
     * @date 2019/9/27 23:51
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    MsgModel msgModel() default MsgModel.AUTO;

}
