package com.chungkui.check.configparser;

import com.chungkui.check.configparser.bean.CheckConfig;

/**
 * @Author: jason
 * @Date: 2020/1/16 10:27
 * @Version 1.0
 */
public interface RulesParser {
    /**
     * 解析并封装checkConfig
     *
     * @param checkConfig  校验配置对象
     * @param localValue   代码本地value数组式校验规则配置
     * @param localConfig  代码本地json字符串式校验规则配置
     * @param remoteConfig 远程json字符串式校验规则配置
     */
    void parsing(CheckConfig checkConfig, String[] localValue, String localConfig, Object remoteConfig);


}
