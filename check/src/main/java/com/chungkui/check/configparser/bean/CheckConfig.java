package com.chungkui.check.configparser.bean;

import com.chungkui.check.expression.MatchExpression;
import com.googlecode.aviator.Expression;

import java.util.List;
import java.util.Map;

/**
 * @Author: jason
 * @Date: 2019/12/31 15:29
 * @Version 1.0
 */
public class CheckConfig {
    private String key;
    private String msgTmp;
    /*校验规则列表*/
    private List<MatchExpression> matchExpressions;
    private ResumitWallConfig resumitWallConfig;
    private TrafficControlConfig trafficControlConfig;
    private DemoteConfig demoteConfig;
    private List<RateLimterConfig> rateLimterConfigs;
    private List<RedisRateLimterConfig> redisRateLimterConfigs;
    private Expression condition;
    public List<RedisRateLimterConfig> getRedisRateLimterConfigs() {
        return redisRateLimterConfigs;
    }

    public void setRedisRateLimterConfigs(List<RedisRateLimterConfig> redisRateLimterConfigs) {
        this.redisRateLimterConfigs = redisRateLimterConfigs;
    }

    public ResumitWallConfig getResumitWallConfig() {
        return resumitWallConfig;
    }

    public void setResumitWallConfig(ResumitWallConfig resumitWallConfig) {
        this.resumitWallConfig = resumitWallConfig;
    }

    public TrafficControlConfig getTrafficControlConfig() {
        return trafficControlConfig;
    }

    public void setTrafficControlConfig(TrafficControlConfig trafficControlConfig) {
        this.trafficControlConfig = trafficControlConfig;
    }

    public DemoteConfig getDemoteConfig() {
        return demoteConfig;
    }

    public void setDemoteConfig(DemoteConfig demoteConfig) {
        this.demoteConfig = demoteConfig;
    }

    public List<RateLimterConfig> getRateLimterConfigs() {
        return rateLimterConfigs;
    }

    public void setRateLimterConfigs(List<RateLimterConfig> rateLimterConfigs) {
        this.rateLimterConfigs = rateLimterConfigs;
    }

    public class RedisRateLimterConfig {
        private String code;
        private String msg;
        /**
         * 会作为reids key前缀
         */
        private String fix;
        /**
         * 令牌桶被填充的速率。
         */
        private int rate;
        /**
         * 令牌桶总容量
         */
        private int capacity;
        /**
         * 一个请求需要要花费令牌个数，默认为1。
         */
        private int perUser;
        /**
         * 动态流控参数
         */
        private String dynamicParam = "";

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getFix() {
            return fix;
        }

        public void setFix(String fix) {
            this.fix = fix;
        }

        public int getRate() {
            return rate;
        }

        public void setRate(int rate) {
            this.rate = rate;
        }

        public int getCapacity() {
            return capacity;
        }

        public void setCapacity(int capacity) {
            this.capacity = capacity;
        }

        public int getPerUser() {
            return perUser;
        }

        public void setPerUser(int perUser) {
            this.perUser = perUser;
        }

        public String getDynamicParam() {
            return dynamicParam;
        }

        public void setDynamicParam(String dynamicParam) {
            this.dynamicParam = dynamicParam;
        }
    }
    public class RateLimterConfig {
        private String code;
        private String msg;
        private String fix;
        private double permitsPerSecond;
        private Long warmupPeriod;
        private Boolean sync;
        private Long waitLimit;
        private String dynamicParam = "";
        /*动态流控对象生成个数流控*/
        private double createRate = 0;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getFix() {
            return fix;
        }

        public double getCreateRate() {
            return createRate;
        }

        public void setCreateRate(double createRate) {
            this.createRate = createRate;
        }

        public void setFix(String fix) {
            this.fix = fix;
        }

        public double getPermitsPerSecond() {
            return permitsPerSecond;
        }

        public void setPermitsPerSecond(double permitsPerSecond) {
            this.permitsPerSecond = permitsPerSecond;
        }

        public Long getWarmupPeriod() {
            return warmupPeriod;
        }

        public void setWarmupPeriod(Long warmupPeriod) {
            this.warmupPeriod = warmupPeriod;
        }

        public Boolean getSync() {
            return sync;
        }

        public void setSync(Boolean sync) {
            this.sync = sync;
        }

        public Long getWaitLimit() {
            return waitLimit;
        }

        public void setWaitLimit(Long waitLimit) {
            this.waitLimit = waitLimit;
        }

        public String getDynamicParam() {
            return dynamicParam;
        }

        public void setDynamicParam(String dynamicParam) {
            this.dynamicParam = dynamicParam;
        }
    }

    public class TrafficControlConfig {
        private String code;
        private String msg;
        private String key;
        private String dynamicParam = "";

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }


        public String getDynamicParam() {
            return dynamicParam;
        }

        public void setDynamicParam(String dynamicParam) {
            this.dynamicParam = dynamicParam;
        }


    }

    public List<MatchExpression> getMatchExpressions() {
        return matchExpressions;
    }

    public void setMatchExpressions(List<MatchExpression> matchExpressions) {
        this.matchExpressions = matchExpressions;
    }

    public class DemoteConfig {
        private String msg;
        private boolean open;
        private String code;

        public String getMsg() {
            return msg;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public boolean isOpen() {
            return open;
        }

        public void setOpen(boolean open) {
            this.open = open;
        }
    }

    public class ResumitWallConfig {
        private Map<String, Object> msgConfig;
        private int expire = 60 * 5;
        private String prefix;
        private String code;

        public Map<String, Object> getMsgConfig() {
            return msgConfig;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public void setMsgConfig(Map<String, Object> msgConfig) {
            this.msgConfig = msgConfig;
        }

        public int getExpire() {
            return expire;
        }

        public void setExpire(int expire) {
            this.expire = expire;
        }

        public String getPrefix() {
            return prefix;
        }

        public void setPrefix(String prefix) {
            this.prefix = prefix;
        }
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }


    public String getMsgTmp() {
        return msgTmp;
    }

    public void setMsgTmp(String msgTmp) {
        this.msgTmp = msgTmp;
    }

    public Expression getCondition() {
        return condition;
    }

    public void setCondition(Expression condition) {
        this.condition = condition;
    }
}
