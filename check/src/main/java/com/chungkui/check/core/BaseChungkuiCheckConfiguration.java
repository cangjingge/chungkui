package com.chungkui.check.core;

import com.chungkui.check.aspect.CheckAspect;
import com.chungkui.check.configparser.RulesParser;
import com.chungkui.check.configparser.impl.RulesParserImpl;
import com.chungkui.check.expression.CheckConfigStore;
import com.chungkui.check.expression.ExpressionFactory;
import com.chungkui.check.handler.CheckHandler;
import com.chungkui.check.handler.impl.*;
import com.chungkui.check.paramparser.ParamParserEngine;
import com.chungkui.check.paramparser.impl.ArgsParamParserEngine;
import com.chungkui.check.paramparser.impl.Ajax3ParamParserEngine;
import com.chungkui.check.paramparser.impl.HttpParamParserEngine;
import com.chungkui.check.paramparser.impl.AjaxParamParserEngine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: jason
 * ChungKui
 * @Date: 2020/1/4 9:37
 * @Version 1.0
 */
@Configuration
public abstract class BaseChungkuiCheckConfiguration {
    public BaseChungkuiCheckConfiguration() {
    }

    public void registerExpression(String type, Class clazz) {
        ExpressionFactory.registerExpression(type, clazz);
    }

    @Bean
    public CheckHandler demoteCheckHandler() {
        return new DemoteCheckHandler();
    }

    @Bean
    public CheckHandler paramCheckHandler() {
        return new ParamCheckHandler();
    }

    @Bean
    public CheckHandler reSubmitWallCheckHandler() {
        return new ReSubmitWallCheckHandler();
    }

    @Bean
    public CheckHandler trafficControlCheckHandler() {
        return new TrafficControlCheckHandler();
    }

    @Bean
    public RateLimterCheckHandler rateLimterCheckHandler() {
        return new RateLimterCheckHandler();
    }

    @Bean
    public ParamParserEngine argsModel() {
        return new ArgsParamParserEngine();
    }

    @Bean
    public ParamParserEngine mvcDeliverModel() {
        return new HttpParamParserEngine();
    }

    @Bean
    public CheckConfigStore checkConfigCacheService(){
        return new CheckConfigStoreImpl(true);
    }

    @Bean
    public ParamParserEngine mvcReturnModel() {
        try {
            Class.forName("org.springframework.web.bind.annotation.RestController");
        } catch (ClassNotFoundException var2) {
            return new Ajax3ParamParserEngine();
        }

        return new AjaxParamParserEngine();
    }

    @Bean
    public MsgBuilder msgBuilder() {
        return new DefaultMsgBuilder();
    }

    @Bean
    public ModelContainer modelContainer() {
        return new ModelContainer();
    }

    @Bean
    public  CheckChain compositeCheck() {
        return new CheckChainImpl();
    }

    @Bean
    public CheckAspect checkAspect() {
        return new CheckAspect();
    }

    @Bean
    public RulesParser rulesParser() {
        return new RulesParserImpl();
    }
}
