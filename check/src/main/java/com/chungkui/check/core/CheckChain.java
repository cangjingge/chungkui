package com.chungkui.check.core;

import com.chungkui.check.configparser.bean.CheckConfig;
import com.chungkui.check.core.bean.CheckResult;

import java.util.Map;

/**
 * 校验链执行接口
 *
 * @Author: jason
 * @Date: 2019/12/30 20:34
 * @Version 1.0
 */
public interface CheckChain {
    /**
     * 执行校验接口
     *
     * @param checkConfig 校验配置
     * @param params      校验入参
     * @return CheckResult
     */
    CheckResult doCheck(CheckConfig checkConfig, Map<String, Object> params);
}
