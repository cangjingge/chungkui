package com.chungkui.check.core;

import org.springframework.beans.factory.annotation.Autowired;
import com.chungkui.check.configparser.bean.CheckConfig;
import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.handler.CheckHandler;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * @Author: jason
 * @Date: 2019/12/30 20:24
 * @Version 1.0
 */

public class CheckChainImpl implements  CheckChain {
    @Autowired
    private List<CheckHandler> handlerChain = new ArrayList<>();
    /**
     * 注入完成后进行排序操作。
     */
   @PostConstruct
    private void init() {
        Collections.sort(handlerChain, Comparator.comparingInt(CheckHandler::sort));
    }

    @Override
    public CheckResult doCheck(CheckConfig checkConfig, Map<String, Object> params) {
            for (CheckHandler handler : handlerChain) {
                CheckResult checkResult = handler.check(checkConfig, params);
                if (!checkResult.ifPass()) {
                    return checkResult;
                }
            }
        return new CheckResult(true);
    }


}
