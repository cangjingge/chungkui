package com.chungkui.check.core;

import com.chungkui.check.paramparser.ParamParserEngine;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: jason
 * @Date: 2019/12/27 19:09
 * @Version 1.0
 */
public class ModelContainer {
    @Autowired
    private List<ParamParserEngine> paramParserEngineList = new ArrayList<>();
    @Autowired
    @Qualifier("argsModel")
    ParamParserEngine defaultParamParserEngine;

    public ParamParserEngine obtainModel(String model, ProceedingJoinPoint joinPoint) {
        if (StringUtils.isNotEmpty(model)) {
            for (ParamParserEngine parserEngine : paramParserEngineList) {
                if (parserEngine.supportsModel(model)) {
                    return parserEngine;
                }
            }
        } else {
            for (ParamParserEngine parserEngine : paramParserEngineList) {
                if (parserEngine.supportsModel(joinPoint)) {
                    return parserEngine;
                }
            }
        }
        return defaultParamParserEngine;
    }
}
