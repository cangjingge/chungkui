package com.chungkui.check.core;

import com.chungkui.check.annotation.Check;
import com.chungkui.check.core.bean.CheckResult;


/**
 * @Author: jason
 * @Date: 2020/1/2 17:21
 * @Version 1.0
 */
public interface MsgBuilder {
    /**
     * 构造异常消息
     *
     * @param result 校验结果对象
     * @param check  配置
     * @return Map<String, Object> 响应消息值
     */
    Object buildFailMsg(CheckResult result, Check check,Class clazz);
    /**
     * 执行指定入参对象校验逻辑
     *
     * @param globalMsgTemplate 校验消息模板
     */
    void setGlobalMsgTemplate(String globalMsgTemplate);
    Object buildSuccessMsg( Check check,Object data);

}
