package com.chungkui.check.core;

public enum MsgModel {
    /**
     * 异常消息提醒模式
     */
    EXCEPTION,
    /**
     * 传递到方法内部模式
     */
    TRANSFER,
    /**
     * 直接返回消息模式
     */
    AJAX,
    /**
     * 普通方法
     */
    RETURN_MAP,
    /**
     * 自适应模式
     */
    AUTO

}
