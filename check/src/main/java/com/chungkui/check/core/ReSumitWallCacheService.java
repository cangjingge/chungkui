package com.chungkui.check.core;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @see [相关类/方法](可选)
 * @since [产品/模块版本] (可选)
 */
public interface ReSumitWallCacheService {

    /**
     * 功能描述: <br>
     * 〈如果存在则返回false,如果不存在则设置并返回true〉
     *
     * @param key
     * @param val
     * @param ex
     * @return boolean
     * @throws
     * @Author jason
     * @date 2019/9/29 10:22
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    boolean set(String key, String val, int ex);

}