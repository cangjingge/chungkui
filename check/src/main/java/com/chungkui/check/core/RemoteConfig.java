package com.chungkui.check.core;

public interface RemoteConfig {
    void change();

    String get(String key, String defaultValue);
}
