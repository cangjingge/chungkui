package com.chungkui.check.core;

public interface RemoteConfigStore {
    /**
     * 远程数据变更回调接口，当人为触发远程配置变更时调用
     *
     * @param key 远程配置key值
     * @param val 远程配置具体值
     */
    void remoteSetCallBack(String key, String val);
    /**
     * 获取远程配置信息接口
     *
     * @param key          远程配置key，必须是唯一的
     * @param defaultValue 如果为空则使用的默认值
     * @return String 获取远程配置信息
     */
    String remoteGet(String key, String defaultValue);
}
