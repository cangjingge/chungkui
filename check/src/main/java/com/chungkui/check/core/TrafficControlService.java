package com.chungkui.check.core;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @see [相关类/方法](可选)
 * @since [产品/模块版本] (可选)
 */
public interface TrafficControlService {
    /**
     * 流控校验接口
     *
     * @param key 流控对象
     * @return boolean 流控校验结果
     */
    boolean check(String key);

    /**
     * 动态流控接口
     *
     * @param common  动态校验key前缀
     * @param dynamic 动态部分值
     * @return boolean 流控校验结果
     */
    boolean dynamicCheck(String common, String dynamic);
}