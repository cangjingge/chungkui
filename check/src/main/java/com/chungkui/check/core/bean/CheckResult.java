
package com.chungkui.check.core.bean;


import java.io.Serializable;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @see [相关类/方法](可选)
 * @since [产品/模块版本] (可选)
 */
public class CheckResult implements Serializable {
    private static final long serialVersionUID = 1L;
    boolean ifPass;
    String code;
    String param;
    String msg;
    String val;

    public boolean isIfPass() {
        return ifPass;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public boolean ifPass() {
        return ifPass;
    }

    public void setIfPass(boolean ifPass) {
        this.ifPass = ifPass;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public CheckResult(boolean ifPass) {
        this.ifPass = ifPass;
    }

    public CheckResult(String code, String param, String msg, String val, boolean ifPass) {
        this.code = code;
        this.param = param;
        this.msg = msg;
        this.val = val;
        this.ifPass = ifPass;
    }

    public CheckResult(String code, String msg, boolean ifPass) {
        this.code = code;
        this.msg = msg;
        this.ifPass = ifPass;
    }

    public CheckResult(boolean ifPass, String msg) {
        this.ifPass = ifPass;
        this.msg = msg;
    }

    public static CheckResult success() {
        return new CheckResult(true);
    }

    public static CheckResult fail(String code, String param, String msg, String val) {
        return new CheckResult(code, param, msg, val, false);
    }

    public static CheckResult fail(String code, String msg) {
        return new CheckResult(code, msg, false);
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}