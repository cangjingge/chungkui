package com.chungkui.check.exception;

import com.chungkui.check.core.bean.CheckResult;

import java.util.Map;

/**
 * @author jason
 * @date 2020/3/28
 */
public class CheckFailException extends RuntimeException {
    private final transient Object jsonMsg;
    private final transient CheckResult checkResult;

    public CheckFailException(Object jsonMsg, CheckResult checkResult) {
        super("chungkui check fail:" + jsonMsg);
        this.checkResult = checkResult;
        this.jsonMsg = jsonMsg;
    }

    public Object getJsonMsg() {
        return jsonMsg;
    }

    public CheckResult getCheckResult() {
        return checkResult;
    }


}
