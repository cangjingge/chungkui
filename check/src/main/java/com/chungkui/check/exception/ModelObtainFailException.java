package com.chungkui.check.exception;
/**
 *
 * @author jason
 * @date 2020/3/28
 */
public class ModelObtainFailException extends Exception {
    public ModelObtainFailException(String message) {
        super(message);
    }
}
