package com.chungkui.check.exception;
/**
 *
 * @author jason
 * @date 2020/3/28
 */
public class RuleParseException extends RuntimeException {
    public RuleParseException(String message) {
        super(message);
    }
}
