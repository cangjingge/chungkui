package com.chungkui.check.expression;

import com.chungkui.check.core.Constants;
import org.apache.commons.collections4.MapUtils;

import java.util.Map;

public abstract class BaseExpression implements MatchExpression {
    protected String normalMsg;
    protected Map<String, Object> msg;

    public String getNormalMsg() {
        return normalMsg;
    }

    public void setNormalMsg(String normalMsg) {
        this.normalMsg = normalMsg;
    }

    public Map<String, Object> getMsg() {
        return msg;
    }

    public void setMsg(Map<String, Object> msg) {
        this.msg = msg;
    }
    @Override
    public void compileMsgConfig(Map<String, Object> config) {
        Object msgConfig = MapUtils.getObject(config, Constants.MSG);
        if (msgConfig instanceof String) {
            this.normalMsg = (String) msgConfig;
        } else if (msgConfig instanceof Map) {
            this.msg = (Map<String, Object>) msgConfig;
        }
    }
}
