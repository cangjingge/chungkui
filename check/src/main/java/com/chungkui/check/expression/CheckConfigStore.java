package com.chungkui.check.expression;

import com.chungkui.check.configparser.bean.CheckConfig;
import com.chungkui.check.annotation.Check;
import com.chungkui.check.core.RemoteConfigStore;

/**
 * @Author: jason
 * @Date: 2019/12/31 17:10
 * @Version 1.0
 */
public interface CheckConfigStore extends RemoteConfigStore {
    /**
     * 根据缓存key获取校验配置信息接口
     *
     * @param key 校验配置
     * @return CheckConfig 校验配置信息
     */
    CheckConfig get(String key);



    /**
     * 清理缓存接口
     */
    void clear();

    /**
     * 解析校验配置
     *
     * @param check 校验注解
     * @param key   缓存key值
     * @return CheckConfig 校验配置
     */
    CheckConfig parseCheckConfig(Check check, String key);




}
