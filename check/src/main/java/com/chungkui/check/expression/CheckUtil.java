
package com.chungkui.check.expression;

import com.chungkui.check.core.Constants;
import com.chungkui.check.core.bean.CheckResult;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @see [相关类/方法](可选)
 * @since [产品/模块版本] (可选)
 */
public class CheckUtil {
    private CheckUtil() {
    }
    public static boolean checkNull(CheckResult result, String val, Map<String, Object> msgMap,String normalMsg) {
        if (StringUtils.isBlank(val)) {
            result.setIfPass(false);
            result.setMsg(MapUtils.getString(msgMap, Constants.NULL, normalMsg));
            return false;
        }
        return true;
    }


    public static CheckResult failResult(CheckResult result, Map<String, Object> msgMap, String key, String msg, String normalMsg) {
        result.setIfPass(false);
        if (StringUtils.isNotEmpty(normalMsg)) {
            result.setMsg(normalMsg);
        } else {
            result.setMsg(MapUtils.getString(msgMap, key, msg));
        }
        return result;
    }

    public static boolean minMaxComparable(CheckResult result, Comparable min, Comparable max, Comparable v, Map<String, Object> msgMap, String normalMsg) {
        if (min != null && v.compareTo(min) < 0) {
            failResult(result, msgMap, Constants.MIN, Constants.MIN_OVER, normalMsg);
            return false;
        }
        if (max != null && v.compareTo(max) > 0) {
            failResult(result, msgMap, Constants.MAX, Constants.MAX_OVER, normalMsg);
            return false;
        }
        return true;
    }

}