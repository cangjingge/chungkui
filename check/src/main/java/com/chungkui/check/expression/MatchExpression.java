
package com.chungkui.check.expression;

import java.util.Map;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.handler.insidecheck.InsideType;

/**
 * 〈一句话功能简述〉<br>
 * 〈编译表达式装饰者类〉
 *
 * @author jason
 * @see [相关类/方法](可选)
 * @since [产品/模块版本] (可选)
 */
public interface MatchExpression {
    /**
     * 执行指定入参对象校验逻辑
     *
     * @param params 入参值
     * @return CheckResult 校验结果
     */
    CheckResult match(Map<String, Object> params);

    void compile(Map<String, Object> config);

    void compileMsgConfig(Map<String, Object> config);

    InsideType getInsideType();
}
