package com.chungkui.check.expression.decorator;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.MatchExpression;
import com.chungkui.check.handler.insidecheck.InsideType;

import java.util.Map;

public class ExpressionDecorator implements MatchExpression {
    private MatchExpression matchExpression;
    public ExpressionDecorator(MatchExpression matchExpression) {
        this.matchExpression = matchExpression;
    }

    @Override
    public CheckResult match(Map<String, Object> params) {
        return matchExpression.match(params);
    }

    @Override
    public void compile(Map<String, Object> config) {
        matchExpression.compile(config);
    }

    @Override
    public void compileMsgConfig(Map<String, Object> config) {
        matchExpression.compileMsgConfig(config);
    }

    @Override
    public InsideType getInsideType() {
        return matchExpression.getInsideType();
    }
}
