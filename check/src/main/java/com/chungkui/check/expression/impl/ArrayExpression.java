package com.chungkui.check.expression.impl;

import com.chungkui.check.core.Constants;
import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.BaseMinMaxExpression;
import com.chungkui.check.expression.CheckUtil;
import com.chungkui.check.handler.insidecheck.InsideType;
import org.apache.commons.collections4.MapUtils;

import java.util.Collection;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @see [相关类/方法](可选)
 * @since [产品/模块版本] (可选)
 */
public class ArrayExpression extends BaseMinMaxExpression {
    @Override
    public CheckResult match(Map<String, Object> params) {
        Object val = MapUtils.getObject(params, this.param);
        if (this.require != null && this.require && val == null) {
            return CheckResult.fail(this.code, this.param, MapUtils.getString(this.msg, Constants.NULL, "不可为空"), "");
        }
        if (val instanceof Collection) {
            Collection<? extends Object> collection = (Collection) val;
            int v = collection.size();
            CheckResult result = new CheckResult(this.code, this.param, null, "", true);
            if (!CheckUtil.minMaxComparable(result, super.min, this.max, v, this.msg, normalMsg)) {
                return result;
            }
        } else {
            return CheckResult.fail(this.code, this.param, "数据类型必须为Collection类型", "");
        }
        return CheckResult.success();

    }

    @Override
    public void compile(Map<String, Object> config) {
        super.init(config);
        setIntegerMinMax(config);
    }

    @Override
    public InsideType getInsideType() {
        return InsideType.ARRAY;
    }
}
