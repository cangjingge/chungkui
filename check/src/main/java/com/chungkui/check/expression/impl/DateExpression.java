package com.chungkui.check.expression.impl;

import com.chungkui.check.core.Constants;
import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.BaseExpression;
import com.chungkui.check.handler.insidecheck.InsideType;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @see [相关类/方法](可选)
 * @since [产品/模块版本] (可选)
 */
public class DateExpression extends BaseExpression {
    public final Logger logger = LoggerFactory.getLogger(DateExpression.class);
    private String param;
    private String fmt;
    private Long after;
    private Long before;
    private Boolean require;
    private String code;

    @Override
    public CheckResult match(Map<String, Object> params) {
        String val = org.apache.commons.collections.MapUtils.getString(params, this.param);
        if (require != null && require && StringUtils.isBlank(val)) {
            return CheckResult.fail(this.code, this.param, MapUtils.getString(msg, Constants.NULL, normalMsg), val);
        } else if (StringUtils.isNotEmpty(val)) {
            try {
                /*校验格式是否满足*/
                DateTimeFormatter formatter = DateTimeFormat.forPattern(fmt);
                DateTime valTime = DateTime.parse(val, formatter);
                DateTime nowTime = DateTime.parse(DateTime.now().toString(formatter), formatter);
                if (after != null) {
                    /*晚于当前时间毫秒数*/
                    DateTime nowAfter = nowTime.plus(after);
                    if (valTime.isBefore(nowAfter)) {
                        return CheckResult.fail(this.code, this.param, MapUtils.getString(msg, "after", normalMsg), val);
                    }
                }
                if (before != null) {
                    DateTime nowBefore = nowTime.plus(before);
                    if (valTime.isAfter(nowBefore)) {
                        return CheckResult.fail(this.code, this.param, MapUtils.getString(msg, "before", normalMsg), val);
                    }
                }
            } catch (Exception e) {
                logger.info("match fail", e);
                return CheckResult.fail(this.code, this.param, MapUtils.getString(msg, Constants.FAIL, MapUtils.getString(msg, "fmt", normalMsg)), val);
            }
        }
        return CheckResult.success();

    }

    @Override
    public void compile(Map<String, Object> config) {
        this.fmt = MapUtils.getString(config, "fmt");
        this.require = MapUtils.getBoolean(config, "require", true);
        this.param = MapUtils.getString(config, "param");
        this.code = MapUtils.getString(config, "code");
        this.after = MapUtils.getLong(config, "after");
        this.before = MapUtils.getLong(config, "before");
    }


    @Override
    public InsideType getInsideType() {
        return InsideType.NO_INSISE;
    }

}
