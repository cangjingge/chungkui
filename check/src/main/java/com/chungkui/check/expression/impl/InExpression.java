package com.chungkui.check.expression.impl;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.BaseExpression;
import com.chungkui.check.handler.insidecheck.InsideType;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @see [相关类/方法](可选)
 * @since [产品/模块版本] (可选)
 */
public class InExpression extends BaseExpression {
    String rule;
    private String code;
    private String param;
    private Boolean require;


    @Override
    public CheckResult match(Map<String, Object> params) {
        String val = MapUtils.getString(params, this.param);
        if (this.require != null && this.require && StringUtils.isBlank(val)) {
            return CheckResult.fail(this.code, this.param, this.normalMsg, val);
        } else if (StringUtils.isEmpty(val) || StringUtils.isEmpty(rule)) {
            return CheckResult.success();
        } else {
            String[] arr = rule.split(",");
            String[] var6 = arr;
            int var7 = arr.length;
            for (int var8 = 0; var8 < var7; ++var8) {
                String s = var6[var8];
                if (StringUtils.equals(s, val)) {
                    return CheckResult.success();
                }
            }
        }
        return CheckResult.fail(this.code, this.param, this.normalMsg, val);
    }

    @Override
    public void compile(Map<String, Object> config) {
        this.rule = MapUtils.getString(config, "rule");
        this.param = MapUtils.getString(config, "param");
        this.code = MapUtils.getString(config, "code");
        this.require = MapUtils.getBoolean(config, "require", true);
    }


    @Override
    public InsideType getInsideType() {
        return InsideType.NO_INSISE;
    }
}
