package com.chungkui.check.expression.impl;

import com.chungkui.check.expression.NumberMinMaxExpression;

import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @see [相关类/方法](可选)
 * @since [产品/模块版本] (可选)
 */
public class IntExpression extends NumberMinMaxExpression {
    @Override
    public Comparable getComparable(String v) {
        return  Integer.parseInt(v.trim());
    }
    @Override
    public void compile(Map<String, Object> config) {
        super.init(config);
        setIntegerMinMax(config);
    }
}
