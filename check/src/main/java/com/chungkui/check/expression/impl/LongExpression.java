package com.chungkui.check.expression.impl;

import com.chungkui.check.expression.NumberMinMaxExpression;
import org.apache.commons.collections4.MapUtils;

import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @see [相关类/方法](可选)
 * @since [产品/模块版本] (可选)
 */
public class LongExpression extends NumberMinMaxExpression {
    @Override
    public Comparable getComparable(String v) {
        return Long.parseLong(v.trim());
    }
    @Override
    public void compile(Map<String, Object> config) {
        super.init(config);
        this.min = MapUtils.getLong(config, "min");
        this.max = MapUtils.getLong(config, "max");
    }
}
