package com.chungkui.check.expression.impl;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.BaseExpression;
import com.chungkui.check.handler.insidecheck.InsideType;
import org.apache.commons.collections4.MapUtils;

import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈对象是否为空校验〉
 *
 * @author jason
 * @see [相关类/方法](可选)
 * @since [产品/模块版本] (可选)
 */
public class NotNullExpression extends BaseExpression {
    private String param;
    private String code;


    @Override
    public CheckResult match(Map<String, Object> params) {
        Object val = MapUtils.getObject(params, this.param);
        if (val == null) {
            return CheckResult.fail(this.code, this.param, this.normalMsg, "");
        }
        return CheckResult.success();
    }

    @Override
    public void compile(Map<String, Object> config) {
        this.param = MapUtils.getString(config, "param");
        this.code = MapUtils.getString(config, "code");
    }


    @Override
    public InsideType getInsideType() {
        return InsideType.NO_INSISE;
    }
}
