package com.chungkui.check.expression.impl;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.BaseMinMaxExpression;
import com.chungkui.check.expression.CheckUtil;
import com.chungkui.check.handler.insidecheck.InsideType;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @see [相关类/方法](可选)
 * @since [产品/模块版本] (可选)
 */
public class RequireExpression extends BaseMinMaxExpression {
    @Override
    public CheckResult match(Map<String, Object> params) {
        String val = MapUtils.getString(params, this.param);
        CheckResult result = new CheckResult(this.code, this.param, null, val, true);
        if (!CheckUtil.checkNull(result, val, this.msg, this.normalMsg)) {
            return result;
        }
        int v = StringUtils.length(val);
        if (!CheckUtil.minMaxComparable(result, this.min, this.max, v, this.msg, normalMsg)) {
            return result;
        }

        return result;

    }

    @Override
    public void compile(Map<String, Object> config) {
        super.init(config);
        setIntegerMinMax(config);
    }


    @Override
    public InsideType getInsideType() {
        return InsideType.NO_INSISE;
    }
}
