package com.chungkui.check.handler;

import com.chungkui.check.configparser.bean.CheckConfig;
import com.chungkui.check.core.bean.CheckResult;

import java.util.Map;

/**
 * 单个校验链接口
 *
 * @Author: jason
 * @Date: 2019/12/30 15:30
 * @Version 1.0
 */
public interface CheckHandler {
    /**
     * 校验参数接口
     *
     * @param checkConfig 校验配置
     * @param params      校验入参
     * @return CheckResult
     */
    CheckResult check(CheckConfig checkConfig, Map<String, Object> params);

    /**
     * 校验链排序值
     *
     * @return int 排序值
     */
    int sort();
}
