package com.chungkui.check.handler.impl;

import com.chungkui.check.configparser.bean.CheckConfig;
import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.handler.CheckHandler;

import java.util.Map;

/**
 * 降级
 * @Author: jason
 * @Date: 2019/12/30 16:51
 * @Version 1.0
 */

public class DemoteCheckHandler implements CheckHandler {
    @Override
    public CheckResult check(CheckConfig checkConfig, Map<String, Object> params) {
        CheckResult checkResult = new CheckResult(true);
        CheckConfig.DemoteConfig demoteConfig = checkConfig.getDemoteConfig();
        if (demoteConfig != null && demoteConfig.isOpen()) {
            checkResult.setMsg(checkConfig.getDemoteConfig().getMsg());
            checkResult.setCode(checkConfig.getDemoteConfig().getCode());
            checkResult.setIfPass(false);
            return checkResult;
        }
        return checkResult;

    }


    @Override
    public int sort() {
        return 1;
    }
}
