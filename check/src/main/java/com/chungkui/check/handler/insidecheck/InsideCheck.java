package com.chungkui.check.handler.insidecheck;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.MatchExpression;

import java.util.List;

/**
 * @author jason
 * @date 2020/3/28
 */
public interface InsideCheck {
    /**
     * 执行指定入参对象校验逻辑
     *
     * @param field            入参值
     * @param matchExpressions 校验表达式列表
     * @return CheckResult 校验结果
     */
    CheckResult check(Object field, List<MatchExpression> matchExpressions);

    /**
     * 是否支持处理该类型参数
     *
     * @param field 入参
     * @param type  参数类型
     * @return boolean 是否支持处理该类型参数
     */
    boolean suport(Object field, InsideType type);

}
