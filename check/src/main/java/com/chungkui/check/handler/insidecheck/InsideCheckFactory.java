package com.chungkui.check.handler.insidecheck;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.MatchExpression;
import com.chungkui.check.handler.insidecheck.impl.*;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * @author jason
 * @date 2020/3/28
 */
public class InsideCheckFactory {
    private static List<InsideCheck> insideChecks = Lists.newArrayList();

    private InsideCheckFactory() {
    }

    static {
        insideChecks.add(new JsonArrayInsideCheckImpl());
        insideChecks.add(new JsonInsideCheckImpl());
        insideChecks.add(new MapInsideCheckImpl());
        insideChecks.add(new IteratorInsideCheckImpl());
        insideChecks.add(new StringInsideCheckImpl());
        insideChecks.add(new BeanInsideCheckImpl());
    }

    public static CheckResult check(Object data, InsideType type, List<MatchExpression> matchExpressions) {
        for (InsideCheck insideCheck : insideChecks) {
            if (insideCheck.suport(data, type)) {
                CheckResult result = insideCheck.check(data, matchExpressions);
                if (!result.ifPass()) {
                    return result;
                }
                break;
            }
        }
        return new CheckResult(true);
    }
}
