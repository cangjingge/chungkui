package com.chungkui.check.handler.insidecheck;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.MatchExpression;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * @author jason
 * @date 2020/3/28
 */
public class ParamCheckUtils {

    private ParamCheckUtils() {
    }

    public static CheckResult check(List<MatchExpression> matchExpressions, Map<String, Object> param) {
        if (CollectionUtils.isNotEmpty(matchExpressions)) {
            for (MatchExpression matchExpression : matchExpressions) {
                CheckResult result = matchExpression.match(param);
                if (!result.ifPass()) {
                    return result;
                }
            }
        }
        return new CheckResult(true);
    }
}
