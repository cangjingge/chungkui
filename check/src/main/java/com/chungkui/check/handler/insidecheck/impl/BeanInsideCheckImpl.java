package com.chungkui.check.handler.insidecheck.impl;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.MatchExpression;
import com.chungkui.check.handler.insidecheck.InsideType;
import com.chungkui.check.handler.insidecheck.InsideCheck;
import com.chungkui.check.handler.insidecheck.ParamCheckUtils;
import com.chungkui.check.util.JsonUtil;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

/**
 * @author jason
 * @date 2020/3/28
 */
public class BeanInsideCheckImpl implements InsideCheck {
    @Override
    public CheckResult check(Object field, List<MatchExpression> matchExpressions) {
        if (CollectionUtils.isNotEmpty(matchExpressions)) {
            CheckResult result = ParamCheckUtils.check(matchExpressions, (JsonUtil.bean2map(field)));
            if (!result.ifPass()) {
                return result;
            }
        }
        return new CheckResult(true);
    }

    /**
     * 非空并且不是字符串
     *
     * @param field 入参
     * @param type  参数类型
     * @return
     */
    @Override
        public boolean suport(Object field, InsideType type) {
        return field != null && !(field instanceof String);
    }
}
