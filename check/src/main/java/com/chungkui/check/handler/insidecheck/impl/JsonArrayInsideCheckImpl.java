package com.chungkui.check.handler.insidecheck.impl;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.MatchExpression;
import com.chungkui.check.handler.insidecheck.InsideType;
import com.chungkui.check.handler.insidecheck.InsideCheckFactory;
import com.chungkui.check.handler.insidecheck.InsideCheck;
import com.chungkui.check.util.JsonUtil;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * @author jason
 * @date 2020/3/28
 */
public class JsonArrayInsideCheckImpl implements InsideCheck {

    @Override
    public CheckResult check(Object field, List<MatchExpression> matchExpressions) {
        if (CollectionUtils.isNotEmpty(matchExpressions)) {
            List<Map<String, Object>> listParam = JsonUtil.jsonToObject((String) field, List.class);
            CheckResult result = InsideCheckFactory.check(listParam, null, matchExpressions);
            if (!result.ifPass()) {
                return result;
            }
        }
        return new CheckResult(true);
    }

    @Override
    public boolean suport(Object field, InsideType type) {
        return InsideType.JSON_ARRAY.equals(type);
    }

}
