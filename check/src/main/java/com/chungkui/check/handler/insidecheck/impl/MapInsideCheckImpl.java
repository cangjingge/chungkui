package com.chungkui.check.handler.insidecheck.impl;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.MatchExpression;
import com.chungkui.check.handler.insidecheck.InsideType;
import com.chungkui.check.handler.insidecheck.InsideCheck;
import com.chungkui.check.handler.insidecheck.ParamCheckUtils;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * @author jason
 * @date 2020/3/28
 */
public class MapInsideCheckImpl implements InsideCheck {

    @Override
    public CheckResult check(Object field, List<MatchExpression> matchExpressions) {
        if (CollectionUtils.isNotEmpty(matchExpressions)) {
            CheckResult result = ParamCheckUtils.check(matchExpressions, (Map<String, Object>) field);
            if (!result.ifPass()) {
                return result;
            }
        }
        return new CheckResult(true);
    }

    @Override
    public boolean suport(Object field, InsideType type)  {
        return field instanceof Map;
    }


}
