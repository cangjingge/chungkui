
package com.chungkui.check.paramparser;

import com.chungkui.check.core.MsgModel;
import org.aspectj.lang.ProceedingJoinPoint;

import java.util.Map;

/**
 * @author jason
 * @date 2020/3/28
 */
public interface ParamParserEngine {
    /**
     * 方法入参解析为map接口，不同模式解析方式不同
     *
     * @param joinPoint 切点对象
     * @return Map<String, Object> 解析结果
     */
    Map<String, Object> parserParam2Map(ProceedingJoinPoint joinPoint);

    /**
     * 校验模式隐式指定时探查接口
     *
     * @param joinPoint 切点对象
     * @return boolean
     */
    boolean supportsModel(ProceedingJoinPoint joinPoint);

    /**
     * 校验模式显式指定时探查接口
     *
     * @param model 显式校验模式
     * @return boolean
     */
    boolean supportsModel(String model);

    /**
     * @return 消息通知模式
     */

    MsgModel getMsgModel();
}
