package com.chungkui.check.paramparser;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: jason
 * @Date: 2020/1/3 10:13
 * @Version 1.0
 */
public class RequestUtil {
    private RequestUtil() {
    }

    public static Map<String, Object> getParam() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        Map<String, String[]> parameter = request.getParameterMap();
        Map<String, Object> rParams = new HashMap<>(0);
        for (Map.Entry<String, String[]> m : parameter.entrySet()) {
            String key = m.getKey();
            Object[] obj = parameter.get(key);
            rParams.put(key, (obj == null || obj.length > 1) ? obj : obj[0]);
        }
        return rParams;
    }
}
