
package com.chungkui.check.paramparser.impl;

import com.chungkui.check.core.MsgModel;
import com.chungkui.check.paramparser.ParamParserEngine;
import com.chungkui.check.paramparser.RequestUtil;
import com.chungkui.check.util.AspectUtil;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈spring4以及以上版本〉
 *
 * @author jason
 * @see [相关类/方法](可选)
 * @since [产品/模块版本] (可选)
 */
public class AjaxParamParserEngine implements ParamParserEngine {
    private final Logger log = LoggerFactory.getLogger(AjaxParamParserEngine.class);

    @Override
    public Map<String, Object> parserParam2Map(ProceedingJoinPoint joinPoint) {
        return RequestUtil.getParam();
    }


    @Override
    public boolean supportsModel(ProceedingJoinPoint joinPoint) {
        try {
            Boolean b1 = AspectUtil.methodHasAnnotation(joinPoint, ResponseBody.class);
            Boolean b2 = AspectUtil.calssHasAnnotation(joinPoint, RestController.class);
            Boolean b3 = AspectUtil.getReturnType(joinPoint) == null;
            return (b1 || b2) && !b3;
        } catch (Exception e) {
            log.error("supportsModel error", e);
        }
        return false;
    }

    @Override
    public boolean supportsModel(String modelType) {
        return StringUtils.equals(modelType, "ajax");
    }

    @Override
    public MsgModel getMsgModel() {
        return MsgModel.AJAX;
    }
}
