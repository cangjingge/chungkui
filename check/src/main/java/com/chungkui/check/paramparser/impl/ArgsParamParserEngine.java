package com.chungkui.check.paramparser.impl;

import com.chungkui.check.core.MsgModel;
import com.google.common.collect.Maps;
import com.chungkui.check.paramparser.ParamParserEngine;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import java.util.Map;

/**
 * Copyright (C), 2002-2019,草帽团开发团队
 * 〈参数列表校验，目前只支持字符串类型〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @fileName: ArgsParamParserEngine.java
 * @date: 2019/6/29 9:31
 * @see [相关类/方法](可选)
 * @since [产品/模块版本] (可选)
 */
public class ArgsParamParserEngine implements ParamParserEngine {
    @Override
    public Map<String, Object> parserParam2Map(ProceedingJoinPoint joinPoint) {
        Object[] argValues = joinPoint.getArgs();
        String[] argNames = ((MethodSignature) joinPoint.getSignature()).getParameterNames();
        Map<String, Object> params = Maps.newHashMap();
        for (int i = 0; i < argNames.length; i++) {
            String an = argNames[i];
            params.put(an, argValues[i]);
        }
        return params;
    }

    /**
     * 此种类型不支持自动探查需要手动指定
     *
     * @param joinPoint
     * @return
     */
    @Override
    public boolean supportsModel(ProceedingJoinPoint joinPoint) {
        return false;
    }

    @Override
    public boolean supportsModel(String modelType) {
        return StringUtils.equals(modelType, "args");
    }

    @Override
    public MsgModel getMsgModel() {
        return MsgModel.RETURN_MAP;
    }


}