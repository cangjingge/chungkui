package com.chungkui.check.util;


import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @see [相关类/方法](可选)
 * @since [产品/模块版本] (可选)
 */
public class AspectUtil {
    private AspectUtil() {
    }

    public static Object[] getArgValues(JoinPoint joinPoint) {
        //参数值
        return joinPoint.getArgs();
    }

    public static Object getArgValue(JoinPoint joinPoint, String name) {
        //参数值
        Object[] argValues = getArgValues(joinPoint);
        String[] argNames = getArgNames(joinPoint);
        for (int i = 0; i < argNames.length; i++) {
            String an = argNames[i];
            if (StringUtils.equals(an, name)) {
                return argValues[i];
            }
        }
        return null;
    }

    public static String[] getArgNames(JoinPoint joinPoint) {
        //参数名称
        return ((MethodSignature) joinPoint.getSignature()).getParameterNames();
    }
    public static Class getReturnType(JoinPoint joinPoint) {
        //参数名称
        return ((MethodSignature) joinPoint.getSignature()).getReturnType();
    }
    /**
     * 判断方法是否包含注解
     *
     * @return
     */
    public static boolean methodHasAnnotation(JoinPoint joinPoint, Class annotationClass) {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        if (method != null) {
            return method.getAnnotation(annotationClass) != null;
        }
        return false;
    }

    /**
     * 判断类是否包含注解
     *
     * @return
     */
    public static boolean calssHasAnnotation(JoinPoint joinPoint, Class annotationClass) {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;

        Method method = methodSignature.getMethod();
        if (method != null) {
            Class clazz = method.getDeclaringClass();
            return clazz.getAnnotation(annotationClass) != null;
        }
        return false;
    }

    /**
     * 判断切点类是否是某个类的子类
     *
     * @return
     */
    public static boolean calssIsExtendsFrom(JoinPoint joinPoint, Class parentClass) {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        if (method != null) {
            Class clazz = method.getDeclaringClass();
            return parentClass.isAssignableFrom(clazz);
        }
        return false;
    }

    /**
     * 判断方法是否包含注解
     *
     * @return
     */
    public static boolean isOneMapParam(JoinPoint joinPoint) {
        Object[] args = getArgValues(joinPoint);
        if (args.length == 1) {
            return args[1] instanceof Map;
        }
        return false;
    }
    public static String getMethodKey(Method method) {
        StringBuilder sb = new StringBuilder();
        sb.append(method.getDeclaringClass().getName());
        sb.append('.');
        sb.append(method.getName());
        sb.append('(');
        separateWithCommas(method.getParameterTypes(), sb);
        sb.append(')');
        return sb.toString();
    }

    public static void separateWithCommas(Class<?>[] types, StringBuilder sb) {
        for (int j = 0; j < types.length; j++) {
            sb.append(types[j].getName());
            if (j < (types.length - 1)) {
                sb.append(",");
            }
        }

    }
}