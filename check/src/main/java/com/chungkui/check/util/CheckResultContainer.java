package com.chungkui.check.util;


/**
 *
 * @author jason
 * @date 2020/3/28
 */
public class CheckResultContainer {
    private static ThreadLocal<Object> mThreadLocal = new ThreadLocal<>();

    private CheckResultContainer() {
    }

    /**
     * 功能描述: <br>
     * 〈校验请求是否通过〉
     *
     * @return boolean
     * @throws
     * @Author jason
     * @date 2019/6/27 15:57
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    public static boolean check() {
        Object object = mThreadLocal.get();
        return object == null;
    }

    /**
     * 功能描述: <br>
     * 〈获取校验失败描述信息〉
     *
     * @return java.lang.Object
     * @throws
     * @Author jason
     * @date 2019/6/27 15:58
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */

    public static Object getFailMsg() {
        Object msg = mThreadLocal.get();
        mThreadLocal.remove();
        return msg;
    }

    /**
     * 功能描述: <br>
     * 〈获取校验失败描述信息〉
     *
     * @return java.lang.Object
     * @throws
     * @Author jason
     * @date 2019/6/27 15:58
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    public static void setFailMsg(Object msg) {
        mThreadLocal.set(msg);
    }


}
