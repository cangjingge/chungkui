package com.chungkui.ratelimiter;

/**
 * 流控对象缓存接口
 *
 * @Author: jason
 * @Date: 2020/1/11 18:30
 * @Version 1.0
 */
public interface RateLimiterCache {
    /**
     * 从缓存中获取静态流控对象
     *
     * @param key 缓存key
     * @return RateLimiterService 流控service对象
     */
    StandAloneRateLimiter get(String key);

    /**
     * 设置静态流控service对象到缓存接口
     *
     * @param key 缓存key
     * @param val 流控service对象
     */
    void set(String key, StandAloneRateLimiter val);

    /**
     * 从缓存中获取动态流控对象
     *
     * @param key 缓存key
     * @return RateLimiterService 流控service对象
     */
    StandAloneRateLimiter getDynamic(String key);

    /**
     * 设置动态流控service对象到缓存接口
     *
     * @param key 缓存key
     * @param val 流控service对象
     */
    void setDynamic(String key, StandAloneRateLimiter val);

    /**
     * 情况流程缓存
     */
    void clear();
}
