package com.chungkui.ratelimiter;

import com.chungkui.ratelimiter.aspect.RateLimiterAspect;
import com.chungkui.ratelimiter.aspect.RedisRateLimiterAspect;
import com.chungkui.ratelimiter.impl.RateLimiterCacheImpl;
import com.chungkui.ratelimiter.impl.StandAloneRatelimiterServiceImpl;
import com.chungkui.ratelimiter.impl.ValueResolveImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RateLimiterConfiguration {
    @Bean
    public RateLimiterAspect rateLimiterAspect() {
        return new RateLimiterAspect();
    }
    @Bean
    public RedisRateLimiterAspect redisRateLimiterAspect(){
        return new RedisRateLimiterAspect();
    }
    @Bean
    public RateLimiterService rateLimiterFlowService() {
        return new StandAloneRatelimiterServiceImpl();
    }

    @Bean
    public ValueResolve valueResolve() {
        return new ValueResolveImpl();
    }

    @Bean
    public RateLimiterCache rateLimiterCacheService() {
        return new RateLimiterCacheImpl();
    }
}
