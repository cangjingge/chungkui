package com.chungkui.ratelimiter;

import com.chungkui.ratelimiter.anotation.RateLimiter;

public interface RateLimiterService {
    void limit(RateLimiter rateLimiter,String key);
    void limit(RateLimiter rateLimiter,String key,String dyParam);
}
