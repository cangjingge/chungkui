package com.chungkui.ratelimiter;

import java.io.IOException;

public interface RedisRateLimiter {
    boolean check(String key, int rate, int capacity, int perUser) throws IOException;
}
