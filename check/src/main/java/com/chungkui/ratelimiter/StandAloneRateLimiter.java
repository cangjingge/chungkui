package com.chungkui.ratelimiter;

/**
 * 流控接口<br>
 *
 * @author JASON
 * @see [相关类/方法](可选)
 * @since [产品/模块版本] (可选)
 */
public interface StandAloneRateLimiter {

    /**
     * 功能描述: <br>
     * 检测能否放行
     *
     * @return true=放行/false=拒绝
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    boolean check();

}
