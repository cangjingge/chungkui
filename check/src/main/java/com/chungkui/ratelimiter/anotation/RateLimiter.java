package com.chungkui.ratelimiter.anotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RateLimiter {
    /**
     * 流控异常码
     *
     * @return
     */
    String code() default "502";

    /**
     * 流控提示
     *
     * @return
     */
    String msg() default "system busy please try later";

    /**
     * 速率x/秒
     *
     * @return
     */
    String rate() ;

    /**
     * 多久预热到最大速率，防止启动时流量过大打死服务器
     *
     * @return
     */
    long warmupPeriod() default 0;

    /**
     * 是否阻塞
     *
     * @return
     */
    boolean block() default false;

    /**
     * 等待超时时间，block为非阻塞的时候有效果
     *
     * @return
     */
    long waitLimit() default 0;

    /**
     * 动态流控参数
     *
     * @return
     */
    String dynamicParam() default "";

    /**
     * 动态流控对象生成个数流控
     *
     * @return
     */
    String createRate() default "0";

}
