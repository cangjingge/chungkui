package com.chungkui.ratelimiter.anotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RedisRateLimiter {
    /**
     * 流控异常码
     *
     * @return
     */
    String code() default "502";

    /**
     * 流控提示
     *
     * @return
     */
    String msg() default "system busy please try later";

    /**
     * 速率x/秒
     *
     * @return
     */
    String rate() ;

    /**
     * 令牌通容量
     *
     * @return
     */
    int capacity() default 50;

    /**
     * 一次获取令牌数
     *
     * @return
     */
    int perUser() default 1;

    /**
     * 等待超时时间，block为非阻塞的时候有效果
     *
     * @return
     */
    long waitLimit() default 0;

    /**
     * 动态流控参数
     *
     * @return
     */
    String dynamicParam() default "";


}
