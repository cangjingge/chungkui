package com.chungkui.ratelimiter.aspect;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.exception.CheckFailException;
import com.chungkui.check.util.AspectUtil;
import com.chungkui.ratelimiter.RedisRateLimiter;
import com.chungkui.ratelimiter.ValueResolve;
import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.lang.reflect.Method;

/**
 * Copyright (C), 2002-2019,草帽团开发团队
 * 〈注解式校验切面类〉<br>
 * 〈功能详细描述〉
 *
 * @author jason
 * @fileName: CheckAspect.java
 * @date: 2019/6/28 12:54
 * @see [相关类/方法](可选)
 * @since [产品/模块版本] (可选)
 */
@Aspect
public class RedisRateLimiterAspect {
    private final Logger log = LoggerFactory.getLogger(RedisRateLimiterAspect.class);
    private LocalVariableTableParameterNameDiscoverer discoverer = new LocalVariableTableParameterNameDiscoverer();
    private ExpressionParser parser = new SpelExpressionParser();

    @Autowired
    private ValueResolve valueResolve;

    public RedisRateLimiterAspect() {
        log.info("chungkui rateLimiter,init success");
    }

    @Autowired(required = false)
    RedisRateLimiter redisRateLimiter;

    @Pointcut("@annotation(com.chungkui.ratelimiter.anotation.RedisRateLimiter)")
    public void checkPointcut() {
        throw new UnsupportedOperationException();
    }

    @Around("checkPointcut()")
    public Object checkAround(ProceedingJoinPoint joinPoint) throws Throwable {
        /*获取注解*/
        com.chungkui.ratelimiter.anotation.RedisRateLimiter redisRateLimiter = getAnnotation(joinPoint);
        String key = getMethodKey(joinPoint);
        if (redisRateLimiter != null) {
            String rate = redisRateLimiter.rate();
            int capacity = redisRateLimiter.capacity();
            int perUser = redisRateLimiter.perUser();
            String dynamicParam = redisRateLimiter.dynamicParam();
            if (StringUtils.isNotEmpty(dynamicParam)) {
                Signature signature = joinPoint.getSignature();
                MethodSignature methodSignature = (MethodSignature) signature;
                Method method = methodSignature.getMethod();
                /*取参数模式*/
                Expression expression = parser.parseExpression(dynamicParam);
                String dyParam = expression.getValue(bindParam(method, joinPoint.getArgs()), String.class);
                key = "chungkui:" + key + ":" + dyParam;
            }
            int rateValue = Integer.parseInt(valueResolve.resolve(rate));
            boolean result = this.redisRateLimiter.check(key, rateValue, capacity, perUser);
            if (!result) {
                throw new CheckFailException(redisRateLimiter.msg(), CheckResult.fail(redisRateLimiter.code(), redisRateLimiter.msg()));
            }

        }
        return joinPoint.proceed();
    }

    private EvaluationContext bindParam(Method method, Object[] args) {
        //获取方法的参数名
        String[] params = discoverer.getParameterNames(method);
        //将参数名与参数值对应起来
        EvaluationContext context = new StandardEvaluationContext();
        for (int len = 0; len < params.length; len++) {
            context.setVariable(params[len], args[len]);
        }
        return context;
    }

    /**
     * 获取方法中的注解信息
     */
    private com.chungkui.ratelimiter.anotation.RedisRateLimiter getAnnotation(JoinPoint joinPoint) {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        if (method != null) {
            return method.getAnnotation(com.chungkui.ratelimiter.anotation.RedisRateLimiter.class);
        }
        return null;
    }

    /**
     * 获取方法中的注解信息
     */
    private String getMethodKey(JoinPoint joinPoint) {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        return AspectUtil.getMethodKey(method);
    }
}
