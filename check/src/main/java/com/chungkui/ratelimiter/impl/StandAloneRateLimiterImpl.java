
package com.chungkui.ratelimiter.impl;

import com.chungkui.ratelimiter.StandAloneRateLimiter;
import com.google.common.util.concurrent.RateLimiter;

import java.util.concurrent.TimeUnit;

/**
 * 基于google guava的单机版本流控.<br>
 * guava的ratelimiter实现的流控.<br>
 *
 * @author jason
 * @see [相关类/方法](可选)
 * @since [产品/模块版本] (可选)
 */
public class StandAloneRateLimiterImpl implements StandAloneRateLimiter {
    private RateLimiter rateLimiter;
    /**
     * 是否直接以阻塞方式执行，默认不阻塞，为true会线程等待直到拿到令牌
     */
    private boolean sync = false;
    /**
     * 阻塞等待时间，0表示不阻塞，取不到直接返回
     */
    private Long waitLimit = 0L;

    /**
     *
     * @param permitsPerSecond 流控量
     * @param warmupPeriod 20
     * @param sync 是否同步等待
     * @param waitLimit 等待超时时间
     */
    public StandAloneRateLimiterImpl(double permitsPerSecond, Long warmupPeriod, Boolean sync, Long waitLimit) {
        if (sync != null) {
            this.sync = sync;
        }
        if (waitLimit != null) {
            this.waitLimit = waitLimit;
        }
        if (warmupPeriod != null) {
            /*WarmingUp方式*/
            rateLimiter = RateLimiter.create(permitsPerSecond, warmupPeriod, TimeUnit.MILLISECONDS);
        } else {
            /*Bursty方式*/
            rateLimiter = RateLimiter.create(permitsPerSecond);
        }
    }

    /**
     * 使用rateLimiter进行判断
     */
    @Override
    public boolean check() {
        if (rateLimiter == null) {
            return true;
        }
        if (!sync) {
            /*非阻塞模式*/
            return rateLimiter.tryAcquire(1, waitLimit, TimeUnit.MILLISECONDS);
        } else {
            /*阻塞模式*/
            rateLimiter.acquire(1);
            return true;
        }

    }


}
