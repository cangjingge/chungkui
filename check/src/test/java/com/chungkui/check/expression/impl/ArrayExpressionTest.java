package com.chungkui.check.expression.impl;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.ExpressionFactory;
import com.chungkui.check.expression.MatchExpression;
import com.chungkui.check.handler.insidecheck.InsideType;
import com.chungkui.check.util.JsonUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class ArrayExpressionTest {
    @Test
    public void array_success_test() {
        Map<String, Object> config = JsonUtil.json2map("{type:'[]',param:'list',min:1,max:2,msg:{min:'长度不足',max:'超长',null:'不可为空'},code:500}");
        MatchExpression matchExpression = new AviatorExpression();
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        List<Map<String, Object>> list = Lists.newArrayList();
        list.add(Maps.newHashMap());
        params.put("list", list);
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertTrue(checkResult.ifPass());
        /*测试边界*/
        list.add(Maps.newHashMap());
        checkResult = matchExpression.match(params);
        Assert.assertTrue(checkResult.ifPass());
        Assert.assertTrue( matchExpression.getInsideType()== InsideType.ARRAY);

    }

    @Test
    public void array_fail_test() {
        Map<String, Object> config = JsonUtil.json2map("{type:'[]',param:'list',min:1,max:2,msg:{min:'长度不足',max:'超长',null:'不可为空'},code:500}");
        MatchExpression matchExpression = new AviatorExpression();
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();

        CheckResult checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "不可为空");
        List<Map<String, Object>> list = Lists.newArrayList();
        params.put("list", list);
        checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "长度不足");
        list.add(Maps.newHashMap());
        list.add(Maps.newHashMap());
        list.add(Maps.newHashMap());
        checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "超长");
        params.put("list", Maps.newHashMap());
        checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "数据类型必须为Collection类型");
    }
}
