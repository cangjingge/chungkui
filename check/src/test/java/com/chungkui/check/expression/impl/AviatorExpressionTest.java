package com.chungkui.check.expression.impl;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.ExpressionFactory;
import com.chungkui.check.expression.MatchExpression;
import com.chungkui.check.handler.insidecheck.InsideType;
import com.chungkui.check.util.JsonUtil;
import com.google.common.collect.Maps;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import static org.junit.Assert.*;

public class AviatorExpressionTest {
    @Test
    public void avt_fail_test() {
        Map<String, Object> config = JsonUtil.json2map("{rule:'str.isEmpty(a)?str.isNotEmpty(b):true',msg:{fail:'a,b不可同时为空'},code:500}");
        MatchExpression matchExpression = new AviatorExpression();
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getCode(), "500");
        Assert.assertEquals(checkResult.getMsg(), "a,b不可同时为空");
        Assert.assertTrue( matchExpression.getInsideType()== InsideType.NO_INSISE);

    }
    @Test
    public void avt_error_fail_test() {
        Map<String, Object> config = JsonUtil.json2map("{rule:'long(a)>4',msg:{error:'非法数字'},code:500}");
        MatchExpression matchExpression = new AviatorExpression();
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getCode(), "500");
        Assert.assertEquals(checkResult.getMsg(), "非法数字");

    }
    @Test
        public void avt_success_test() {
        Map<String, Object> config = JsonUtil.json2map("{rule:'str.isEmpty(a)?str.isNotEmpty(b):true',msg:'a,b不可同时为空',code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        params.put("a", "a");
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertTrue(checkResult.ifPass());

    }
}
