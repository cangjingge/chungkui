package com.chungkui.check.expression.impl;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.ExpressionFactory;
import com.chungkui.check.expression.MatchExpression;
import com.chungkui.check.handler.insidecheck.InsideType;
import com.chungkui.check.util.JsonUtil;
import com.google.common.collect.Maps;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import static org.junit.Assert.*;

public class DateExpressionTest {
    @Test
    public void date_success_test() {
        Map<String, Object> config = JsonUtil.json2map("{param:'date',type:'date',fmt:'yyyy-MM-dd HH:mm:ss',msg:{fmt:'时间格式不对',null:'时间不可为空'},code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        params.put("date", "2021-01-09 19:17:20");
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertTrue(checkResult.ifPass());
        Assert.assertTrue( matchExpression.getInsideType()== InsideType.NO_INSISE);

    }
    @Test
    public void date_require_fail_test() {
        Map<String, Object> config = JsonUtil.json2map("{param:'date',type:'date',fmt:'yyyy-MM-dd HH:mm:ss',msg:'时间不可为空',code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        params.put("date", "");
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "时间不可为空");
    }
    @Test
    public void date_fail_test() {
        Map<String, Object> config = JsonUtil.json2map("{param:'date',type:'date',fmt:'yyyy-MM-dd HH:mm:ss',msg:{fmt:'时间格式不对',null:'时间不可为空'},code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        params.put("date", "2021-01-09 19:17:2a");
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "时间格式不对");
    }

    @Test
    public void date_before_test() {
        Map<String, Object> config = JsonUtil.json2map("{param:'date',before:-100,type:'date',fmt:'yyyy-MM-dd HH:mm:ss',msg:{fmt:'时间格式不对',before:'必须早于当前时间',null:'时间不可为空'},code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        DateTime dateTime = new DateTime();
        params.put("date", dateTime.toString("yyyy-MM-dd HH:mm:ss"));
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "必须早于当前时间");
        params.put("date", "1992-03-26 12:12:12");
        checkResult = matchExpression.match(params);
        Assert.assertTrue(checkResult.ifPass());
    }

    @Test
    public void date_after_test() {
        Map<String, Object> config = JsonUtil.json2map("{param:'date',after:100,type:'date',fmt:'yyyy-MM-dd HH:mm:ss'," +
                "msg:{fmt:'时间格式不对',after:'必须晚于当前时间',null:'时间不可为空'},code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        DateTime dateTime = new DateTime();
        System.out.println(dateTime);
        params.put("date", dateTime.toString("yyyy-MM-dd HH:mm:ss"));
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "必须晚于当前时间");
        params.put("date", dateTime.plus(60000).toString("yyyy-MM-dd HH:mm:ss"));
        checkResult = matchExpression.match(params);
        Assert.assertTrue(checkResult.ifPass());
    }
}
