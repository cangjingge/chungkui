package com.chungkui.check.expression.impl;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.ExpressionFactory;
import com.chungkui.check.expression.MatchExpression;
import com.chungkui.check.handler.insidecheck.InsideType;
import com.chungkui.check.util.JsonUtil;
import com.google.common.collect.Maps;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import static org.junit.Assert.*;

public class DateIntervalMatchExpressionTest {
    @Test
    public void date_success_test() {
        Map<String, Object> config = JsonUtil.json2map("{start:'startTime',end:'endTime',minInterval:60000,maxInterval:'120000',type:'dateInterval',fmt:'yyyy-MM-dd HH:mm:ss',msg:{minInterval:'跨度小了',maxInterval:'跨度大了',null:'时间不可为空'},code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        params.put("startTime", "2021-01-09 19:17:20");
        params.put("endTime", "2021-01-09 19:18:20");
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertTrue(checkResult.ifPass());
        Assert.assertTrue( matchExpression.getInsideType()== InsideType.NO_INSISE);

    }

    @Test
    public void date_fail_test() {
        Map<String, Object> config = JsonUtil.json2map("{start:'startTime',end:'endTime',minInterval:60000,maxInterval:" +
                "'120000',type:'dateInterval',fmt:'yyyy-MM-dd HH:mm:ss',msg:{minInterval:'跨度小了',maxInterval:'跨度大了',null:'时间不可为空'},code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        params.put("startTime", "2021-01-09 19:17:20");
        params.put("endTime", "2021-01-09 19:23:20");
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "跨度大了");
        params.put("startTime", "2021-01-09 19:17:20");
        params.put("endTime", "2021-01-09 19:17:21");
        checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "跨度小了");
    }
    @Test
    public void date_require_fail_test() {
        Map<String, Object> config = JsonUtil.json2map("{start:'startTime',end:'endTime',minInterval:60000,maxInterval:" +
                "'120000',type:'dateInterval',fmt:'yyyy-MM-dd HH:mm:ss',msg:{minInterval:'跨度小了',maxInterval:'跨度大了',null:'时间不可为空'},code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        params.put("endTime", "2021-01-09 19:23:20");
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "时间不可为空");
    }

    @Test
    public void date_not_require_success_test() {
        Map<String, Object> config = JsonUtil.json2map("{start:'startTime',end:'endTime',require:false,minInterval:60000,maxInterval:" +
                "'120000',type:'dateInterval',fmt:'yyyy-MM-dd HH:mm:ss',msg:{minInterval:'跨度小了',maxInterval:'跨度大了',null:'时间不可为空'},code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        params.put("endTime", "2021-01-09 19:23:20");
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertTrue(checkResult.ifPass());
    }
}
