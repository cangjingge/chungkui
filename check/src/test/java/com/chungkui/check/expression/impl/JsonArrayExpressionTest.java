package com.chungkui.check.expression.impl;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.ExpressionFactory;
import com.chungkui.check.expression.MatchExpression;
import com.chungkui.check.handler.insidecheck.InsideType;
import com.chungkui.check.util.JsonUtil;
import com.google.common.collect.Maps;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import static org.junit.Assert.*;

public class JsonArrayExpressionTest {
    @Test
    public void jsonArr_success_test() {
        Map<String, Object> config = JsonUtil.json2map("{param:'users',type:'[json]',min:'2',max:'5',msg:{min:'太短',max:'太长',null:'时间不可为空'},code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        params.put("users", "[{name:'jason'},{name:'cjy'}]");
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertTrue(checkResult.ifPass());
        Assert.assertTrue( matchExpression.getInsideType()== InsideType.JSON_ARRAY);

    }

    @Test
    public void jsonArr_fail_test() {
        Map<String, Object> config = JsonUtil.json2map("{param:'users',type:'[json]',min:'2',max:'5',msg:{min:'太短',max:'太长',null:'空',fail:'格式不正确'},code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        params.put("users", "");
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "空");
        params.put("users", "dddd");
        checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "格式不正确");
        params.put("users", "[]");
        checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "太短");
        params.put("users", "[{name:'jason'},{name:'cjy'},{name:'jason'},{name:'cjy'},{name:'jason'},{name:'cjy'},{name:'jason'},{name:'cjy'}]");
        checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "太长");
    }

    @Test
    public void jsonArr_not_require_test() {
        Map<String, Object> config = JsonUtil.json2map("{param:'users',require:false,type:'[json]',min:'2',max:'5',msg:{min:'太短',max:'太长',null:'空',fail:'格式不正确'},code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        params.put("users", "");
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertTrue(checkResult.ifPass());
        params.put("users", "dddd");
        checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "格式不正确");
        params.put("users", "[]");
        checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "太短");
        params.put("users", "[{name:'jason'},{name:'cjy'},{name:'jason'},{name:'cjy'},{name:'jason'},{name:'cjy'},{name:'jason'},{name:'cjy'}]");
        checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "太长");
    }
}
