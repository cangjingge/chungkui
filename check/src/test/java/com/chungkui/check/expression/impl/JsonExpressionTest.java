package com.chungkui.check.expression.impl;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.ExpressionFactory;
import com.chungkui.check.expression.MatchExpression;
import com.chungkui.check.handler.insidecheck.InsideType;
import com.chungkui.check.util.JsonUtil;
import com.google.common.collect.Maps;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import static org.junit.Assert.*;

public class JsonExpressionTest {
    @Test
    public void json_success_test() {
        Map<String, Object> config = JsonUtil.json2map("{param:'user',type:'(json)',msg:{fail:'格式错误',null:'用户必传'},code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        params.put("user", "{name:'jason'}");
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertTrue(checkResult.ifPass());
        Assert.assertTrue( matchExpression.getInsideType()== InsideType.JSON_OBJECT);

    }
    @Test
    public void json_fail_test() {
        Map<String, Object> config = JsonUtil.json2map("{param:'user',type:'(json)',msg:{fail:'格式错误',null:'用户必传'},code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        params.put("user", "{name:'jason}");
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(),"格式错误");
    }
    @Test
    public void json_require_fail_test() {
        Map<String, Object> config = JsonUtil.json2map("{param:'user',type:'(json)',msg:{fail:'格式错误',null:'用户必传'},code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();

        CheckResult checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(),"用户必传");
    }
    @Test
    public void json_not_require_success_test() {
        Map<String, Object> config = JsonUtil.json2map("{param:'user',require:false,type:'(json)',msg:'fail',code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertTrue(checkResult.ifPass());
    }
}
