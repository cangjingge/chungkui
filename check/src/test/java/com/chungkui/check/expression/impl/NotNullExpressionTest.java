package com.chungkui.check.expression.impl;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.ExpressionFactory;
import com.chungkui.check.expression.MatchExpression;
import com.chungkui.check.handler.insidecheck.InsideType;
import com.chungkui.check.util.JsonUtil;
import com.google.common.collect.Maps;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import static org.junit.Assert.*;

public class NotNullExpressionTest {

    @Test
    public void notNull_success_test() {
        Map<String, Object> config = JsonUtil.json2map("{param:'obj',type:'notNull',msg:'空',code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        params.put("obj", "4");
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertTrue(checkResult.ifPass());
        Assert.assertTrue( matchExpression.getInsideType()== InsideType.NO_INSISE);

    }
    @Test
    public void notNull_fail_test() {
        Map<String, Object> config = JsonUtil.json2map("{param:'obj',type:'notNull',msg:'空',code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        params.put("obj", null);
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(),"空");
    }
}
