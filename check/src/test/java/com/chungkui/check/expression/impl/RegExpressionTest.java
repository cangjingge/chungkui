package com.chungkui.check.expression.impl;

import com.chungkui.check.core.bean.CheckResult;
import com.chungkui.check.expression.ExpressionFactory;
import com.chungkui.check.expression.MatchExpression;
import com.chungkui.check.handler.insidecheck.InsideType;
import com.chungkui.check.util.JsonUtil;
import com.google.common.collect.Maps;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import static org.junit.Assert.*;

public class RegExpressionTest {
    @Test
    public void reg_success_test() {
        Map<String, Object> config = JsonUtil.json2map("{param:'num',type:'reg',rule:'^1(3|4|5|7|8)\\\\d{9}$',msg:{fail:'格式错误',null:'时间不可为空'},code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        params.put("num", "13145204569");
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertTrue(checkResult.ifPass());
        Assert.assertTrue(matchExpression.getInsideType() == InsideType.NO_INSISE);

    }

    @Test
    public void reg_fail_test() {
        Map<String, Object> config = JsonUtil.json2map("{param:'num',type:'reg',rule:'^1(3|4|5|7|8)\\\\d{9}$',msg:{fail:'格式错误',null:'时间不可为空'},code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        params.put("num", "131452045649");
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "格式错误");
    }

    @Test
    public void reg_require_test() {
        Map<String, Object> config = JsonUtil.json2map("{param:'num',require:false,type:'reg',rule:'^1(3|4|5|7|8)\\\\d{9}$',msg:{fail:'格式错误',null:'时间不可为空'},code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertTrue(checkResult.ifPass());
    }

    @Test
    public void reg_require_true_test() {
        Map<String, Object> config = JsonUtil.json2map("{param:'num',type:'reg',rule:'^1(3|4|5|7|8)\\\\d{9}$',msg:'时间不可为空',code:500}");
        MatchExpression matchExpression = null;
        try {
            matchExpression = ExpressionFactory.buildExpression(config);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        Map<String, Object> params = Maps.newHashMap();
        CheckResult checkResult = matchExpression.match(params);
        Assert.assertFalse(checkResult.ifPass());
        Assert.assertEquals(checkResult.getMsg(), "时间不可为空");
    }
}
