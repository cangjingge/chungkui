package com.chungkui;

import com.chungkui.check.annotation.EnableCheck;
import com.chungkui.ratelimiter.anotation.EnableRateLimiter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableCheck
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
