package com.chungkui.demo.mvc;

import com.chungkui.check.annotation.Check;
import com.chungkui.demo.bean.Response;
import com.chungkui.demo.bean.UserInfo;
import com.chungkui.demo.service.MainService;
import com.chungkui.ratelimiter.anotation.RateLimiter;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author jason
 */
@Controller
public class MainController {
    private static String success = "200";
    @Autowired
    MainService mainService;



    @GetMapping(path = "/")
    @RateLimiter(rate = "${rrrr}",warmupPeriod = 1)
    public String index(ModelMap map) {
        return "index";
    }
    @PostMapping(path = "/require")
    @ResponseBody
    @Check(value = {"{param:'param',type:'require',code:500,min:2,max:5,msg:{min:'长度不足',max:'超长了',null:'不可为空'}}"}
    ,condition = "str.equals('save',editType)")
    public Response require(String param) {
        return new Response();
    }

    @PostMapping(path = "/datetest")
    @ResponseBody
    @Check(value = {"{param:'startTime',require:true,type:'date',fmt:'yyyy-MM-dd',code:500,after:0,msg:'日期不合法'}"})
    public Object datetest(String param) {
        return success;
    }
    @PostMapping(path = "/number")
    @ResponseBody
    @Check({"{param:'param',type:'double',code:500,min:10,max:20,msg:{min:'不可小于10',max:'不可大于20',fail:'数字不合法',null:'不可为空'}}"})
    public Object number() {
        return success;
    }

    @PostMapping(path = "/avt")
    @ResponseBody
    @Check(value = {"{condition:'str.isNotEmpty(a)',rule:'long(a)>long(b)',msg:{fail:'校验不通过',error:'数字格式不正确'},code:500}"})
    public Object avt() {
        return success;
    }

    @PostMapping(path = "/avt1")
    @ResponseBody
    @Check({"{rule:'str.isEmpty(a)?str.isNotEmpty(b):true',msg:{fail:'a为空字符串b不可为空'},code:500}"})
    public Object avt1() {
        return success;
    }

    @PostMapping(path = "/ratelimiter")
    @ResponseBody
    @Check(redisRateLimters={"{capacity:1,rate:1,perUser:1,msg:'您手速太快了,请稍后再试',code:500}"},
    rateLimters = {"{rate:1,msg:'您手速太快了,请稍后再试',code:500}"} )
    public Object ratelimiter() {
        return success;
    }

    @PostMapping(path = "/ratelimiterdy")
    @ResponseBody
    @Check(rateLimters = {"{rate:1,param:'a',msg:'您手速太快了,请稍后再试',code:500,sync:true}"})
    public Object ratelimiterdy() {
        return success;
    }

    @PostMapping(path = "/ratelimiterdycreate")
    @ResponseBody
    @Check(rateLimters = {"{rate:100,createRate:1,param:'a',msg:'用户量激增，流控命中',code:500}"})
    public Object ratelimiterdycreate() {
        return success;
    }

    @PostMapping(path = "/array")
    @ResponseBody
    @Check(rules = "[" +
            "{param:'param',type:'[json]',min:1,max:5,code:'500',msg:{min:'数组长度不足',max:'超长了',null:'数组不可为空'}," +
            "rules:[{param:'a',type:'require',min:2,max:5,msg:{min:'长度不足',max:'超长了',null:'不可为空'},code:500}]}" +
            "]")
    public Object arraytest() {
        return success;
    }


    @PostMapping(path = "/complextest")
    @ResponseBody
    @Check(rules = "[" +
            "{param:'userinfo',type:'(json)',require:true,msg:'json格式不正确',code:'500'," +
            "rules:[{param:'username',type:'require',code:500,min:2,max:5,msg:{min:'用户名太短',max:'用户名太长',null:'不可为空'}}]}" +
            "]")
    public Object complextest() {
        return success;
    }

    /**
     * 没有入口直接通过浏览器访问url进行测试
     * http://127.0.0.1:8080/testargs?param1=1qq
     *
     * @param param1
     * @param param2
     * @return
     */
    @GetMapping(path = "/testargs")
    @ResponseBody
    public Object testargs(String param1, String param2) {
        return mainService.test1(param1, param2);
    }

    /**
     * 没有入口直接通过浏览器访问url进行测试
     * http://127.0.0.1:8080/testargs?param1=1qq
     *
     * @return
     */
    @GetMapping("/testargs2")
    @ResponseBody
    public Object testargs2() {
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("jason");
        return mainService.test2(userInfo);
    }

    @GetMapping("/testargs3")
    @ResponseBody
    public Object testargs3() {
        List list = Lists.newArrayList();
        UserInfo userInfo = new UserInfo();
        list.add(userInfo);
        return mainService.testArr(list);
    }

    @GetMapping(path = "/in")
    @ResponseBody
    @Check({"{param:'param',rule:'01,02,03',type:'in',code:500,min:2,max:5,msg:'校验不通过'}"})
    public Object in(String param) {
        return success;
    }

    @PostMapping(path = "/phone")
    @ResponseBody
    @Check({"{param:'param',require:true,type:'reg',rule:'^1(3|4|5|7|8)\\\\d{9}$',code:500,msg:{fail:'格式不正确',null:'不可为空'}}"})
    public Object phone(String param) {
        return success;
    }
}
