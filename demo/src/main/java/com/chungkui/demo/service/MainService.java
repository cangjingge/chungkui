package com.chungkui.demo.service;

import com.chungkui.demo.bean.UserInfo;

import java.util.List;
import java.util.Map;

public interface MainService {
    Map test1(String param1, String param2);

    Map test2(UserInfo userInfo);

    Map testArr(List<UserInfo> data);
}
