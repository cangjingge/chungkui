package com.chungkui.demo.service.impl;

import com.chungkui.check.annotation.Check;
import com.chungkui.demo.bean.UserInfo;
import com.chungkui.demo.service.MainService;
import com.google.common.collect.Maps;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class MainServiceImpl implements MainService {
    @Override
    @Check({"{param:'param1',type:'require',code:500,min:2,max:5,msg:{min:'长度不足',max:'超长了',null:'不可为空'}}"})
    public Map test1(String param1, String param2) {
        Map result = Maps.newHashMap();
        result.put("code", "200");
        return result;
    }

    @Override
    @Check(rules = "[" +
            "{field:'userInfo',require:true,code:'500',msg:'userinfo对象不可为空'," +
            "rules:[{param:'username',type:'require',min:2,max:5,msg:{min:'长度不足',max:'超长了',null:'不可为空'},code:500}]}" +
            "]")
    public Map test2(UserInfo userInfo) {
        Map result = Maps.newHashMap();
        result.put("code", "success");
        return result;
    }

    @Override
    @Check(rules = "[" +
            "{field:'data[]',min:1,max:5,code:'500',msg:{min:'数组长度不足',max:'超长了',null:'数组不可为空'}," +
            "rules:[{param:'username',type:'require',min:2,max:5,msg:{min:'长度不足',max:'超长了',null:'不可为空'},code:500}]}" +
            "]")
    public Map testArr(List<UserInfo> data) {
        Map result = Maps.newHashMap();
        result.put("code", "202");
        return result;
    }
}
