var test = function () {

}
test.prototype = {
    init: function () {
        this.bindtest($(".requiretest"), "/require");
        this.bindtest($(".phonetest"), "/phone");
        this.bindtest($(".numbertest"), "/number");
        this.bindtest($(".avttest"), "/avt");
        this.bindtest($(".avttest1"), "/avt1");
        this.bindtest($(".datetest"), "/datetest");

        this.arrayTest();
        this.limiterTest();
        this.limiterTestDy();
        this.limiterTestCreate();
        this.complextest();
        this.reteflag = false;
        this.reteflagdy = false;
        this.reteflagdycreate = false;
    },
    bindtest: function (el, url) {
        var _this = this;
        el.find("button").click(function () {
            var params = _this.serializeJson(el.find("form"));
            $.ajax({
                url: url,
                type: 'POST',
                dataType: "json",
                data: params,
                cache: false,
                complete: function (res) {
                    el.find(".message").val(JSON.stringify(res.responseText))
                }
            })
        })
    },
    complextest: function () {
        var el = $(".complextest");
        el.find("button").click(function () {
            var param = el.find("textarea[name=param]").val();
            $.ajax({
                url: "/complextest",
                type: 'POST',
                dataType: "json",
                data: {userinfo: param},
                cache: false,
                traditional: true,
                complete: function (res) {
                    el.find(".message").val(JSON.stringify(res.responseText))
                }
            })
        })

    },
    arrayTest: function () {
        var el = $(".arraytest");
        el.find("button").click(function () {
            $.ajax({
                url: "/array",
                type: 'POST',
                dataType: "json",
                data: {param: el.find("input[name=param]").val()},
                cache: false,
                complete: function (res) {
                    el.find(".message").val(JSON.stringify(res.responseText))
                }
            })
        })
    },
    limiterTestCreate: function () {
        var _this = this;
        var el = $(".ratelimiter_dy_create");
        el.find("button").click(function () {
            _this.reteflagdycreate = !_this.reteflagdycreate
            var rate = parseInt(el.find("input[name=rate]").val());
            _this.limiterTestRateDyCreate(rate, el);
        })
    },
    limiterTestRateDyCreate: function (rate, el) {
        var _this = this;
        if (!_this.reteflagdycreate) {
            return
        }
        setTimeout(function () {
            $.ajax({
                url: "/ratelimiterdycreate",
                type: 'POST',
                dataType: "json",
                data: {a: el.find("input[name=a]").val()},
                cache: false,
                complete: function (res) {
                    var data = res.responseText;
                    var param = parseInt(el.find("input[name=a]").val()) + 1;
                    el.find("input[name=a]").val(param);

                    if (data == '200') {

                        el.find("button").removeClass("text-danger");
                        el.find("button").addClass("text-success");
                    } else {
                        el.find("button").addClass("text-danger");
                        el.find("button").removeClass("text-success");
                    }
                    el.find(".message").val(JSON.stringify(res.responseText))
                }
            })
            _this.limiterTestRateDyCreate(rate, el);
        }, 1000 / rate);
    },
    limiterTestDy: function () {
        var _this = this;
        var el = $(".ratelimiter_dy");
        el.find("button").click(function () {
            _this.reteflagdy = !_this.reteflagdy
            var rate = parseInt(el.find("input[name=rate]").val());
            _this.limiterTestRateDy(rate, el);
        })
    },
    limiterTestRateDy: function (rate, el) {
        var _this = this;
        if (!_this.reteflagdy) {
            return
        }
        setTimeout(function () {
            $.ajax({
                url: "/ratelimiterdy",
                type: 'POST',
                dataType: "json",
                data: {a: el.find("input[name=a]").val()},
                cache: false,
                complete: function (res) {
                    var data = res.responseText;
                    var param = parseInt(el.find("input[name=a]").val());
                    if (data == '200') {
                        el.find("button").removeClass("text-danger");
                        el.find("button").addClass("text-success");
                    } else {
                        el.find("input[name=a]").val(param + 1);
                        el.find("button").addClass("text-danger");
                        el.find("button").removeClass("text-success");
                    }
                    el.find(".message").val(JSON.stringify("param:" + param + ":" + res.responseText))
                }
            })
            _this.limiterTestRateDy(rate, el);
        }, 1000 / rate);
    },
    limiterTest: function () {
        var _this = this;
        var el = $(".ratelimiter");
        el.find("button").click(function () {
            _this.reteflag = !_this.reteflag;
            var rate = parseInt(el.find("input[name=rate]").val());
            _this.limiterTestRate(rate, el);
        })
    },

    limiterTestRate: function (rate, el) {
        var _this = this;
        if (!_this.reteflag) {
            return
        }
        setTimeout(function () {
            $.ajax({
                url: "/ratelimiter",
                type: 'POST',
                dataType: "json",
                data: {},
                cache: false,
                complete: function (res) {
                    var data = res.responseText;
                    if (data == '200') {
                        el.find("button").removeClass("text-danger");
                        el.find("button").addClass("text-success");
                    } else {
                        el.find("button").addClass("text-danger");
                        el.find("button").removeClass("text-success");
                    }
                    el.find(".message").val(JSON.stringify(res.responseText))
                }
            })
            _this.limiterTestRate(rate, el);
        }, 1000 / rate);
    },
    serializeJson: function (targetFormEl) {
        var o = {};
        var a = targetFormEl.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    }
}
$(function () {
    new test().init()
});
